import React from 'react';
import '../styles/TagsSelect.css';
import { taskTags } from '../assets/Tags';

const TagsSelect = (props) => {
  const [tagOptions, setTagOptions] = React.useState([{}]);

  React.useState(() => {
    let tagsArray = [];
    for (const [key, value] of Object.entries(taskTags)) {
      tagsArray.push({
        name: key,
        value: value
      });
    };
    setTagOptions(tagsArray);
  }, []);

  const deleteTag = (e, option) => {
    props.setSelectedOptions(() =>  props.selectedOptions.filter((element) => element !== option))
  }

  const handleSelect = (evt) => {
    if (!props.selectedOptions.includes(evt.target.value)) {
      return props.setSelectedOptions([...(props.selectedOptions), evt.target.value])
    }
  };

  return (
    <div className="tagsSelect">
      <div className="tagsSelect__tags-cont">
        <h2 className="tagsSelect__title">Tags:</h2>
        <select multiple className="tagsSelect__select-tag" onChange={handleSelect} value={props.selectedOptions}>
          {
            tagOptions.map((tagOption, index) => {
              return (
                <option className="tagsSelect__select-tag-option" value={tagOption.value} key={index}>
                  { tagOption.value }
                </option>)
            })
          }
        </select>
      </div>
      <div className="tagsSelect__selected-tags-cont">
        <h2 className="tagsSelect__title">Selected tags:</h2>
          <div className="tagsSelect__selected-tags-inner-cont">
            {
              props.selectedOptions.map((option, index) => {
                return (
                  <div className="tagsSelect__single-tag-cont" key={index}>
                    <label className="tagsSelect__tag-label" >
                      {option}                     
                    </label>
                    <div className="tagsSelect__tag-label-cancel" onClick={(e) => deleteTag(e, option)}></div>
                  </div>                 
                )
              })
            }
          </div>
        </div>
    </div>
  )
};

export default TagsSelect;