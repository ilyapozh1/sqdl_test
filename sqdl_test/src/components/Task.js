import React from 'react';
import '../styles/Task.css';

const Task = (props) => {
  return (
    <div className="task">
      <p className="task__id">{props.index}</p>
      <p className="task__description">{props.description}</p>
      <div className="task__tags-cont">
        {
          props.tags?.map((tag, index) => {
            const tagValue = tag;
            return (
              <div className="task__tag" key={index}>
                <p className="task__tag-label">{tagValue}</p>
              </div>
            );
          })
        }
      </div>
      <p className="task__completed"></p>
      <input 
        type="checkbox" 
        className="task__completed-checkbox"
        checked={props.completed}
        onChange={() => props.checkCompleted(props.id, props.completed)}></input>
      <button 
        className="task__delete-button"
        onClick={() => props.deleteTask(props.id)}
        >Delete</button>
    </div>
  )
}

export default Task;