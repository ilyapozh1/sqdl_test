import React from 'react';
import '../styles/TaskForm.css';
import { taskTags } from '../assets/Tags';
import TagsSelect from './TagsSelect';

const TaskForm = (props) => {

  const [tagOptions, setTagOptions] = React.useState([{}]);
  const [selectedOptions, setSelectedOptions] = React.useState([]);
  const [description, setDescription] = React.useState('');

  React.useState(() => {
    let tagsArray = [];
    for (const [key, value] of Object.entries(taskTags)) {
      tagsArray.push({
        name: key,
        value: value
      });
    };
    setTagOptions(tagsArray);
  }, []);

  const addTask = () => {
    props.addTaskToList(description, selectedOptions);
    setSelectedOptions([]);
    setDescription('');
  };

  return (
    <div className="taskForm">
      <div className="taskForm__main-cont">
        <div className="taskForm__description-cont">
          <h2 className="taskForm__title">Description:</h2>
          <input className="taskForm__description-input" value={description} onChange={(e) => setDescription(e.target.value)}></input>
        </div>
        <TagsSelect selectedOptions={selectedOptions} setSelectedOptions={setSelectedOptions}/>
      </div>     
      <button className="taskForm__add-task-button" onClick={addTask}>Add task</button>
    </div>
  )
}

export default TaskForm;