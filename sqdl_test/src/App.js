import React from 'react';
import './styles/App.css';
import { INITIAL_TASK_LIST } from './assets/InitialTasks';
import Task from './components/Task';
import TaskForm from './components/TaskForm';
import TagsSelect from './components/TagsSelect';

function App() {

  const [taskArray, setTaskArray] = React.useState([...INITIAL_TASK_LIST]);
  const [filteredArray, setFilteredArray] = React.useState([{}]);
  const [taskFormOpen, setTaskFormOpen] = React.useState(false);
  const [selectedOptions, setSelectedOptions] = React.useState([]);
  const [showCompleted, setShowCompleted] = React.useState(true);

  const handleAddTaskClick = () => {
    setTaskFormOpen(true);
  };

  const addTaskToList = (description, tags) => {
    if (description.length > 0) {
      setTaskArray(() => [...taskArray, {
        id: taskArray.length + Math.floor((Math.random()*100)), // generate random id's for unique values
        tags: [...tags],
        description: description,
        completed: false
      }])
    };
    setTaskFormOpen(false);
  };

  const deleteTask = (id) => {
    setTaskArray(() => taskArray.filter((task) => task.id !== id))
  };

  const handleCheckCompleted = (id, completed) => {
    setTaskArray((taskArray) => taskArray.map((task) => {
      if (task.id === id) {
        return {
          ...task,
          completed: !completed
        }
      }
      return task
    }))
  };

  React.useEffect(() => {   
    if (selectedOptions.length > 0 ) {      
     return setFilteredArray(() => taskArray.filter((task) => {
        for (const [key] in selectedOptions) {         
          if (!task.tags.includes(selectedOptions[key]) || (showCompleted ? false : task.completed)) {
            return false; // Skip the element if any condition is not met
          }
        }
        return true;
      }));
    };
    if (!showCompleted) {
      return setFilteredArray(() => taskArray.filter((task) => !task.completed))
    }
  }, [selectedOptions, showCompleted, taskArray]);

  return (
    <div className="App">
      <div className="App__main-cont">
        <h1 className="App__title">Task list</h1>       
        <div className="App__task-form-cont">
          <button className="App__add-button" onClick={handleAddTaskClick}>Add new task</button>
          {
            taskFormOpen ? <TaskForm addTaskToList={addTaskToList}/> : <></>
          }
        </div>
        <div className="App__filters-cont">
          <h2>Filters</h2>
          <div className="App__filter-inner-cont">
            <TagsSelect selectedOptions={selectedOptions} setSelectedOptions={setSelectedOptions}/>
            <div className="App__completed-filter-checkbox-cont">
              <h2 className="App__title App__title_compl-filter">Show completed tasks:<input 
                type="checkbox" 
                className="App__completed-checkbox-filter"
                checked={showCompleted}
                onChange={() => setShowCompleted(!showCompleted)}
              >
              </input></h2>
              
            </div>
          </div>
        </div>       
        <div className="App__tasks-cont">
          {(selectedOptions.length > 0 || !showCompleted) ? 
            (filteredArray.length > 0 ? filteredArray.map((task, index) => {
              return <Task 
                      id={task?.id} 
                      key={task?.id} 
                      tags={task?.tags}
                      index={index + 1}
                      description={task?.description} 
                      completed={task?.completed}
                      deleteTask={deleteTask}
                      checkCompleted={handleCheckCompleted}
                    />
              }) : <></>) :
            (taskArray.length > 0 ? taskArray.map((task, index) => {
                return <Task 
                        id={task?.id} 
                        key={task?.id} 
                        tags={task?.tags}
                        index={index + 1}
                        description={task?.description} 
                        completed={task?.completed}
                        deleteTask={deleteTask}
                        checkCompleted={handleCheckCompleted}
                      />
              }) : <></>)  
            }
        </div>
      </div>
    </div>
  );
}

export default App;
