import { taskTags } from '../assets/Tags';

export const INITIAL_TASK_LIST = [
  {
    id: 100,
    tags: [
      taskTags.productManagement,
      taskTags.inventoryManagement,
      taskTags.aBTesting,
      taskTags.customerService, 
    ],
    description: "Lorem ipsum",
    completed: true
  },
  {
    id: 200,
    tags: [
      taskTags.salesAndMarketing,
      taskTags.customerService,
      taskTags.websiteDevelopment,
      taskTags.orderFulfillment, 
    ],
    description: "Ipsum lorem",
    completed: false
  },
  {
    id: 300,
    tags: [
      taskTags.orderFulfillment,
      taskTags.paymentProcessing,
      taskTags.productManagement,
      taskTags.socialMediaManagement,
      taskTags.contentCreation, 
    ],
    description: "Lorem ipsum Ipsum lorem",
    completed: true
  },
  {
    id: 400,
    tags: [
      taskTags.searchEngineOptimization,
      taskTags.advertisingAndPromotions,
      taskTags.pricingStrategy,
      taskTags.analyticsAndReporting,
      taskTags.supplierManagement,
    ],
    description: "Lorem ipsum Ipsum lorem Ipsum lorem",
    completed: false
  },
  {
    id: 500,
    tags: [
      taskTags.customerRelationshipManagement,
      taskTags.userExperienceDesign,
      taskTags.conversionRateOptimization,
      taskTags.dataAnalysis, 
    ],
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    completed: true
  },
  {
    id: 600,
    tags: [
      taskTags.customerRelationshipManagement,
      taskTags.socialMediaManagement,
      taskTags.analyticsAndReporting,
      taskTags.dataAnalysis, 
    ],
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    completed: false
  },
  {
    id: 700,
    tags: [
      taskTags.orderFulfillment,
      taskTags.conversionRateOptimization,
    ],
    description: "Lorem ipsum",
    completed: true
  },
  {
    id: 800,
    tags: [
      taskTags.inventoryManagement,
      taskTags.dataAnalysis, 
    ],
    description: "LLorem ipsum dolor sit amet, consectetur adipiscing elit",
    completed: false
  },
  {
    id: 900,
    tags: [
      taskTags.customerRelationshipManagement,
      taskTags.userExperienceDesign,
      taskTags.conversionRateOptimization,
      taskTags.dataAnalysis, 
    ],
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    completed: true
  },
  {
    id: 1000,
    tags: [
      taskTags.shippingAndLogistics,
      taskTags.pricingStrategy,
      taskTags.conversionRateOptimization,
      taskTags.dataAnalysis, 
    ],
    description: "Lorem ipsum",
    completed: false
  },
];